package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Student;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.StudentInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.StudentRepository;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.StudentDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders.StudentBuilder;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Course;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.CourseRepository;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.CourseDTO;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class StudentService {
  
    private final StudentRepository studentRepo;
    private final CourseRepository courseRepo;
    
    private List<Student> Students = new LinkedList<>();
        
    public List<Student> getAllStudent(){
        return Students;
    }

    public StudentDTO getOneStudentCPF(String cpf){
        List<StudentDTO> getAllStudent = studentRepo.findAll();
        for (StudentDTO student : getAllStudent) {
            if(student.getCpf().equals(cpf)){
                return student;
        }   
        }
        return null;
    }

    public Course getStudentCourse(UUID id){
        Student student = studentRepo.getReferenceById(id);
        Course course = student.getCourse();
        return course;
    }      

    public Student addStudent(StudentInput studentInput) {
        Course course = courseRepo.getReferenceById(studentInput.getCourseId());
        Student student = StudentBuilder.build(studentInput);
        student.setCourse(course);
        studentRepo.save(student);
        return student;
    }
   
    public StudentDTO changeStudentCourse(String cpf, UUID graduationId){
        List<Student> getAllStudent = studentRepo.getAllStudentCPF();
        Course course = courseRepo.getReferenceById(courseId);
        for (Student student : Students) {
            if(student.getCpf().equals(cpf)){
                return student;
        }   
        }
        return null;
    }
}


