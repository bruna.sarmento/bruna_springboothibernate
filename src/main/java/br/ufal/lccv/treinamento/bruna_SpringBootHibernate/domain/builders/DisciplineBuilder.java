package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Discipline;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.DisciplineInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.DisciplineDTO;

public class DisciplineBuilder {
        
    public static Discipline build(DisciplineInput input) {
        return Discipline.builder()
                .name(input.getName())
                .workload(input.getWorkload())
                .classroom(input.getClassroom())
                .description(input.getDescription())
                .build();
    }
    public static DisciplineDTO build(Discipline discipline) {
        return DisciplineDTO.builder()
                .name(discipline.getName())
                .workload(discipline.getWorkload())
                .classroom(discipline.getClassroom())
                .description(discipline.getDescription())
                .build();
    }


}
