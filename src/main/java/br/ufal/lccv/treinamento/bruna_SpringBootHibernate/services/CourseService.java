package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Course;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.CourseInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.CourseRepository;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.CourseDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders.CourseBuilder;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Student;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.StudentRepository;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Professor;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.ProfessorRepository;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Discipline;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.DisciplineRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class CourseService {
 
    private List<Course> Courses = new LinkedList<>();

    private final CourseRepository courseRepo;
    

    public List<Course> getAllCourse(){
        return courseRepo.getAllCourse();
    }

    public Course getOneCourse(String name){
        List<Course> getAllCourse = courseRepo.getAllCourse();
        for (Course course : getAllCourse) {
            if(course.getCourse == name){
                return course;
        }   
        }
        return null;
    }

    public Course addCourse(CourseInput courseInput) {
        Course course = CourseBuilder.build(courseInput);
        course = courseRepo.save(course);

        return course;
    }

    public List<Student> getAllStudents(UUID id){
        Course course = courseRepo.getReferenceId(id);
        return course.getStudents();
    }

    public Student getCourseStudent(UUID id){
        Course course = courseRepo.getReferenceId(id);
        Student student = course.getStudent();
        return student;
    }     

}
