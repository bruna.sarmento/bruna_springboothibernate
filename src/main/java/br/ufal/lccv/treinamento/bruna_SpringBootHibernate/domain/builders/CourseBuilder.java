package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Course;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.CourseInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.CourseDTO;

public class CourseBuilder {
        
    public static Course build(CourseInput input) {
        return Course.builder()
                .name(input.getName())
                .coordinatorId(input.getCoordinatorId())
                .creationDate(input.getCreationDate())
                .build();
    }
    public static CourseDTO build(Course course) {
        return CourseDTO.builder()
                .name(course.getName())
                .coordinatorId(course.getCoordinatorId())
                .creationDate(course.getCreationDate())
                .build();
    }
}
