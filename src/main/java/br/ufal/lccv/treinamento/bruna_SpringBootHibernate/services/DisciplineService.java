package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services;


import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Discipline;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.DisciplineInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.DisciplineRepository;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.DisciplineDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders.DisciplineBuilder;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Professor;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.ProfessorRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class DisciplineService {
 
    private final DisciplineRepository disciplineRepo;
    private final ProfessorRepository professorRepo;
    private List<Discipline> Disciplines = new LinkedList<>();

    public List<Discipline> getAllDiscipline(){
        return disciplineRepo.getAllDiscipline();
    }

    public Discipline getOneDiscipline(String name){
        List<Discipline> getAllDiscipline = disciplineRepo.getAllDiscipline();
        for (Discipline discipline : getAllDiscipline) {
            if(discipline.getName == name){
                return discipline;
        }       
        }
        return null;
    }

    public Discipline addDiscipline(DisciplineInput disciplineInput) {
        Discipline discipline = DisciplineBuilder.build(disciplineInput);
        discipline = disciplineRepo.save(discipline);

        return DisciplineBuilder.build(discipline);
    }

    public Professor getDisciplineProfessor(UUID id){
        Discipline discipline = disciplineRepo.getReferenceId(id);
        Professor professor = discipline.getProfessor();
        return professor;
    }     

    public DisciplineDTO changeProfessorDiscipline(UUID professorId, UUID disciplineId){
        Professor professor = professorRepo.getReferenceId(professorId);
        Discipline discipline = disciplineRepo.getReferenceId(professorId);
        
        discipline = disciplineRepo.save(discipline);
        return DisciplineBuilder.build(discipline);
    }
}
