package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.UUID;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor

public class StudentDTO {

    private UUID studentId;
    private String cpf;
    private String name;
    private String email;
    private String phone;
    private String CourseId;
}
