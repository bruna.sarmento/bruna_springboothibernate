package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import java.util.List;
import java.util.UUID;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Professor {
    
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type="uuid-char")
    private UUID id;

    private String cpf;
    private String name;
    private String email;
    private String phone;
    private String degreeOfTraining;

    @OneToMany(mappedBy = "disciplineProfessor")
    private List<Discipline> disciplines;

    @OneToMany(mappedBy = "courseProfessor")
    private List<Course> courses;

    public void addProfessor(Professor professor) {
    }
}
