package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class CourseDTO {

    private UUID id;
    private String name;
    private String coordinatorId;
    private String creationDate;
}
